""" video tutorial in russian on
https://www.youtube.com/watch?v=6Szh-qz4jvw&list=PLlKID9PnOE5hCuNW8L-qxC12U7WPWG6YS&index=7 """

import pytest
import shutil
import os
from pathlib import Path


# scope='session', autouse=True means the function will use before and after all tests
# Code before yield will be used before tests and after yield - after all tests
@pytest.fixture(scope='session', autouse=True)
def get_test_db() -> None:
    """
    The function replace real data to test ones for test.
    If there is no any real data test data use temporary and then disappear.
    In the process backup of real data is creating if it existed in the beginning.
    Warning: test data is located in test/test_db directory.
    DO NOT change the location and do not delete it!
    """
    if Path('habit_info.csv').exists() and Path('habit_time.csv').exists():
        # backup real data
        shutil.copy('habit_info.csv', 'habit_info_temp.csv')
        shutil.copy('habit_time.csv', 'habit_time_temp.csv')

        # replace real data to test data
        shutil.copy('test/test_db/habit_info.csv', 'habit_info.csv')
        shutil.copy('test/test_db/habit_time.csv', 'habit_time.csv')
    else:
        # getting test data
        shutil.copy('test/test_db/habit_info.csv', 'habit_info.csv')
        shutil.copy('test/test_db/habit_time.csv', 'habit_time.csv')

    yield
    # replace test data to real data
    if Path('habit_info_temp.csv').exists() and Path('habit_time_temp.csv').exists():
        shutil.copy('habit_info_temp.csv', 'habit_info.csv')
        shutil.copy('habit_time_temp.csv', 'habit_time.csv')
        os.remove('habit_info_temp.csv')
        os.remove('habit_time_temp.csv')
    else:
        # deleting temporary test data
        os.remove('habit_info.csv')
        os.remove('habit_time.csv')
