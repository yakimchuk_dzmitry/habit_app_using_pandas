"""Module contains special exceptions that help to logic for another parts of program """


class WrongTime(Exception):
    pass


class WrongHabitName(Exception):
    pass


class WrongHabitId(Exception):
    pass


class WrongHabitStatus(Exception):
    pass


class NoData(Exception):
    pass
